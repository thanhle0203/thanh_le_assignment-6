/**************************************************************************************************
 Thanh Le
 SJSU ID: 015062801
 Instruction: Prof. Ramin Moazeni
 CIS 49J
 Homework #6
 Oct 21, 2020

 * Description: This appointment book test program will create save() method to save the appointments data
 * to a file and reload the data from a file. Then create the Junit test class to test for occursOn() method,
 * save() method, and load() method.
 ***************************************************************************************************/

package P9_5;
import org.junit.jupiter.api.Test;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import static org.junit.jupiter.api.Assertions.*;

public class P9_5Test {
    AppointmentBook book = new AppointmentBook();

    @Test
    void occursOn() {
        //Appointment a = new Appointment(2020, 10, 15);
        Onetime o = new Onetime(2020, 10, 15, "See a doctor");
        Daily d = new Daily(2020, 10, 15, "Blood Test");
        Monthly m = new Monthly(2020, 10, 15, "Covid Test");

        //Check date 2020-10-15, all 3 should occur
        assertTrue(o.occursOn(2020, 10, 15));
        assertTrue(d.occursOn(2020, 10, 15));
        assertTrue(m.occursOn(2020, 10, 15));

        //Check date 2020-11-15, daily adn monthly should occur
        assertFalse(o.occursOn(2020, 11, 15));
        assertTrue(d.occursOn(2020, 11, 15));
        assertTrue(m.occursOn(2020, 11, 15));

        //Check date 2020-12-05, only daily should occur
        assertFalse(o.occursOn(2020, 12, 15));
        assertTrue(d.occursOn(2020, 12, 15));
        assertFalse(m.occursOn(2020, 12, 05));

        //Check date 2020-09-15, none would occur since before the date that appointment is made
        assertFalse(o.occursOn(2020, 07, 15));
        assertFalse(d.occursOn(2020, 07, 15));
        assertFalse(m.occursOn(2020, 07, 15));
    }

    @Test
    void save() throws FileNotFoundException, IOException {
        AppointmentBook book = new AppointmentBook();
        //Get new appointment object constructor
        Onetime o = new Onetime(2020, 10, 15, "See a doctor");
        Daily d = new Daily(2020, 10, 15, "Blood Test");
        Monthly m = new Monthly(2020, 10, 15, "Covid Test");

        //Add appointments
        book.addAppointment(o);
        book.addAppointment(d);
        book.addAppointment(m);

        //Save appointment to the file
        book.save("appointments.txt", o);
        book.save("appointments.txt", d);
        book.save("appointments.txt", m);


        //Test if appointments are added
        assertEquals("See a doctor on 2020 - 10 - 15 [Onetime]", o.toString());
        assertEquals("Blood Test on 2020 - 10 - 15 [Daily]", d.toString());
        assertEquals("Covid Test on 2020 - 10 - 15 [Monthly]", m.toString());
        File file = new File("E:\\CS49J\\Assignment-6\\appointments.txt");
        assertTrue(file.exists());

        //Test if appointments are saved
        File fileInput = new File("appointments.txt");
        assertTrue(fileInput.exists());

    }

    @Test
    void load() throws FileNotFoundException, IOException{
        AppointmentBook book = new AppointmentBook();
        //Get new appointment object constructor
        Onetime o = new Onetime(2020, 10, 15, "See a doctor");
        Daily d = new Daily(2020, 10, 15, "Blood Test");
        Monthly m = new Monthly(2020, 10, 15, "Covid Test");

        //Add appointments
        book.addAppointment(o);
        book.addAppointment(d);
        book.addAppointment(m);

        //Save appointments to the file
        book.save("savedApps.txt", o);
        book.save("savedApps.txt", d);
        book.save("savedApps.txt", m);


        //Test for correct number Appointments
        assertTrue(book.getNumAppointment()==3);
        //assertTrue(book.load("appointments.txt"));

        //Test if appointments are loaded (i.e compare appointment object with content of the file
        assertNotEquals("[See, a, doctor, on, 2020, -, 10, -, 15, [Onetime], Blood, Test, on, 2020, -, 10, -, 15, [Daily], Covid, Test, on, 2020, -, 10, -, 15, [Monthly]]", book.load("savedApps.txt"));
        assertEquals("[See, a, doctor, on, 2020, -, 10, -, 15, [Onetime]", book.load("savedApps.txt"));
    }
}
