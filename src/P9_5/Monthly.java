package P9_5;

import java.time.LocalDate;

public class Monthly extends Appointment {
    public Monthly(int year, int month, int day){
        super(year, month, day);
    }
    public Monthly(int year, int month, int day, String description){
        super(year, month, day, description);
    }
    //Method check Monthly Occur
    public boolean occursOn(int year, int month, int day){
        LocalDate d1 = LocalDate.of(getYear(), getMonth(), getDay());
        LocalDate d2 = LocalDate.of(year, month, day);
        //Method check if the day is the same as the day of the date of the appointment
        //and if the input is after the date of after the date of appointment
        if ((day==getDay()&&d2.isAfter(d1))||d2.isEqual(d1)){
            return true;
        }else{
            return false;
        }
    }
    //Method return Monthly datetime and description
    public String toString(){
        return super.toString() + " [Monthly]";
    }
}
