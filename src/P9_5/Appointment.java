package P9_5;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Appointment{
    //Declare date time variables
    private int year;
    private int month;
    private int day;
    private String description;
    private String path;
    private boolean append_to_file = false;

    //Constructor of the Appointment class
    public Appointment(int year, int month, int day){
        this.year = year;
        this.month = month;
        this.day = day;
    }

    //Constructor of the Appointment class
    public Appointment(int year, int month, int day, String description){
        this.year = year;
        this.month = month;
        this.day = day;
        this.description = description;
    }
    //Method get Year() return year of Date
    public int getYear(){
        return year;
    }
    //Method get Month() return month of Date
    public int getMonth(){
        return month;
    }
    //Method get Day() return day of Date
    public int getDay(){
        return day;
    }
    //Method get Day return day of Date
    public boolean occursOn(int year, int month, int day){
        if (getYear()==year&&getMonth()==month&&getDay()==day){
            return true;
        } else {
            return false;
        }
    }
    //Method getDescription() return description for Appointment
    public String getDescription(){
        return description;
    }
    //Method toString() return string of Appointment
    public String toString(){
        String date = getYear() + " - " + getMonth() + " - " + getDay();
        return getDescription() + " on " + date;
    }

}
