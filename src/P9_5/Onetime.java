package P9_5;

public class Onetime extends Appointment{
    public Onetime(int year, int month, int day, String description){
        super(year, month, day, description);
    }

    @Override
    public boolean occursOn(int year, int month, int day) {
        return super.occursOn(year, month, day);
    }

    //Method return Onetime datetime and description
    public String toString(){
        return super.toString() + " [Onetime]";
    }
}
