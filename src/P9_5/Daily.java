package P9_5;

import java.time.LocalDate;

public class Daily extends Appointment {
    //Constructor Daily Class
    public Daily(int year, int month, int day, String description){
        super(year, month, day, description);
    }
    //Check appointments occur
    public boolean occursOn(int year, int month, int day){
        //Daily datetime
        LocalDate d1 = LocalDate.of(getYear(), getMonth(), getDay());
        //Appointment datetime
        LocalDate d2 = LocalDate.of(year, month, day);
        //check if the input is the date of the appointment or after
        if (d2.isEqual(d1)||d2.isAfter(d1))
            return true;
        else
            return false;
    }
    //Method return Daily datetime and description
    public String toString(){
        return super.toString() + " [Daily]";
    }
}
