package P9_5;

import java.io.IOException;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class P9_5 {
    public static void main(String[] args) throws IOException {;
        //New object of class AppointmentBook
        AppointmentBook book = new AppointmentBook();

        //Get input object from Scanner
        Scanner input = new Scanner(System.in);
        char option;
        do{
            System.out.println("Select an option: \n" +
                    " + A for add an appointment, " +
                    " + C for checking, " +
                    " + R for Read File Appointment," +
                    " + Q to quick: ");
            option = input.next().toUpperCase().charAt(0);    //Character option input
            switch(option){
                case 'A':
                    System.out.println("Enter the type (O - Onetime, D - Daily, or M - Monthly): ");
                    char type;
                    //Catch invalid type
                    try{
                        type = input.next().toUpperCase().charAt(0);
                        if (type=='O'||type=='D'||type=='M'){
                            //Catch date out of bounds
                            try{
                                input.nextLine();
                                System.out.println("Enter the date (yyyy-mm-dd): ");
                                String date = input.nextLine();
                                String[] dateParts = date.split("-");
                                int year = Integer.parseInt(dateParts[0]);  //get year integer
                                int month = Integer.parseInt(dateParts[1]); //get month integer
                                int day = Integer.parseInt(dateParts[2]);   //get day integer

                                System.out.println("Enter the description: ");
                                String description = input.nextLine();

                                if (type=='O'){
                                    Onetime o = new Onetime(year, month, day, description);
                                    //Add appointments of Onetime to the array list
                                    book.addAppointment(o);
                                    book.save("appointments.txt", o);

                                }else if(type=='D'){
                                    Daily d = new Daily(year, month, day, description);
                                    //Add appointments of Monthly to the array list
                                    book.addAppointment(d);
                                    book.save("appointments.txt", d);

                                }else if(type=='M'){
                                    Monthly m = new Monthly(year, month, day, description);
                                    //Add appointments of Monthly to the array list
                                    book.addAppointment(m);
                                    book.save("appointments.txt", m);
                                }

                            }catch (ArrayIndexOutOfBoundsException e){
                                System.out.println("Invalid Date: Out of bounds");
                            }
                        }else{
                            throw new InputMismatchException("Invalid type input");
                        }

                    }catch (Exception e){
                        System.out.println("Invalid type input");
                    }

                    book.toStringBook();
                    break;
                case 'C':
                    //catch invalid date
                    try{
                        System.out.println("Enter year: ");
                        int year = input.nextInt();
                        System.out.println("Enter month: ");
                        int month = input.nextInt();
                        System.out.println("Enter day: " );
                        int day = input.nextInt();

                        book.toShow(year, month, day);

                    }catch(DateTimeException e){
                        System.out.println("Invalid date input: ");
                    }catch(InputMismatchException e){
                        System.out.println("Invalid date input: ");
                    }
                    break;
                case 'R':
                    System.out.println("Appointments: ");
                    System.out.println(book.load("appointments.txt"));
                    break;
                case 'Q':
                    break;
                default:
                    System.out.println("Invalid input option");
            }
        }while(option!='Q');

    }
}
