package P9_5;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Iterator;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.IOException;

public class AppointmentBook {
    //New object of ArrayList appointment
    private ArrayList<Appointment> book;
    public AppointmentBook(){
        book = new ArrayList<Appointment>();
    }
    public void addAppointment(Appointment list){
        book.add(list);
    }
    public int getNumAppointment(){
        return book.size();
    }
   // public void getAppointment(){return book;}

    public void removeAppointment(int year, int month, int day){
        //Get the iterator from the list
        Iterator<Appointment> bookIterator = book.iterator();
        //Iterate through the list
        while(bookIterator.hasNext()){
            Appointment appointment = bookIterator.next();

            //if the appointment matches, remove that appointment
            if(appointment.getYear()==year&&appointment.getMonth()==month&&appointment.getYear()==year)
            {
                bookIterator.remove();
            }
        }
    }

    //Check if the appointment object occurs
    public boolean occursOn(int year, int month, int day){
        for(Appointment a: book){
            if (a.getYear()==year&&a.getMonth()==month&&a.getDay()==day)
                return true;
        }
        return false;
    }


    //Print occur appointments
    public void toShow(int year, int month, int day){
        for (Appointment a: book){
            if (a.occursOn(year, month, day)){
                System.out.println(a.toString());
            }
        }

    }
    public void toStringBook(){
        //String result = " \n";
        for(Appointment a: book){
            String result = a.toString();
            System.out.println(result);

        }
    }
    //Method save will write appointment to a file
    public static void save(String file, Appointment appointment) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
        writer.append("\n");
        writer.append(appointment.toString());
        System.out.println();
        writer.close();
    }
    //Method load() return the String ArrayList from the input fileName
    public static ArrayList<String> load(String fileName) throws FileNotFoundException {
        //Create String list of ArrayList
        ArrayList<String> list= new ArrayList<>();

        //Read the input file
        File input = new File(fileName);
        Scanner inputFile = new Scanner(input);

        //Read each String line in the file input
        while(inputFile.hasNext()){
            String line = inputFile.next();
            line = line.trim();
            list.add(line);
        }
        inputFile.close();
        return list;
    }
}
