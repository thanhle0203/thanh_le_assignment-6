/**************************************************************************************************
 Thanh Le
 SJSU ID: 015062801
 Instruction: Prof. Ramin Moazeni
 CIS 49J
 Homework #6
 Oct 21, 2020

 * Description: This recursion indexOf() method will return the index of the String str equals
 * the substring of text or return -1 if they don't equal.
 ***************************************************************************************************/

package E13_9;

public class E13_9 {
    public static void main(String[] args) {
        System.out.println(indexOf("Mississippi", "sip"));
        System.out.println(indexOf("Mississippi", "siss"));
        System.out.println(indexOf("Mississippi", "pi"));
        System.out.println(indexOf("Mississippi", "Mis"));
        System.out.println(indexOf("Mississippi", "ssissi"));
        System.out.println(indexOf("Mississippi", "sisi"));
        System.out.println(indexOf("Mississippi", "isi"));
        System.out.println(indexOf("", "sip"));
        System.out.println(indexOf("Mississippi", ""));
        System.out.println(indexOf("", ""));
    }
    //Method indexOf check the length of text and str
    public static int indexOf(String text, String str){
        //Return 0 if length of text equals length of str
        if (text.length()==0&&str.length()==0){
            return 0;
        }else if (text.length()==0){
            return -1;
        }else if(str.length()==0){
            return 0;
            //Return -1 if length of text less than str
        }else if(text.length()<str.length()){
            return -1;
        }else{
            //Recursion of indexOfHelp method with the third helper argument
            return indexOfHelp(text, str, 0);
        }

    }
    //Method indexOfHelp() recursion with the helper third index return index or -1
    public static int indexOfHelp(String text, String str, int index){
        //Return -1 if length of (text-str)<-1
        if (text.length() - str.length() < index)
            return -1;
        //Return index when the string of text at the index position equal the string of str
        if (text.substring(index, index + str.length()).equals(str))
            return index;
        else
            //Increasing index of the method indexofHelp() recursion
            return indexOfHelp(text, str, index + 1);
    }
}
